import React, { Component } from 'react';
import Layout from './hoc/layout/layout';
import Editor from './containers/editor/editor';


class App extends Component {
  render() {
    return (
      <div>
        <Layout>
          <Editor />
        </Layout>
      </div>
    );
  }
}

export default App;
