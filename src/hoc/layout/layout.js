import React from 'react';
import './layout.css';
// import Aux from '../aux/aux';

class Layout extends React.Component {

    render () {
        return (
            <main className="Content">
                {this.props.children}
            </main>
        )
    }
}

export default Layout;