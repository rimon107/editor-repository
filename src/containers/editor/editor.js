import React from 'react';
import Aux from '../../hoc/aux/aux';
import Toolbar from '../../components/toolbar/toolbar';
import Notebook from '../../components/notebook/notebook';
import Input from '../../components/ui/input/input';
import Action from '../../components/action/action';
// import sanitizeHtml from "sanitize-html";

const COLOR = {
    selected: "black",
    unselected: "white"
};

const SELECTED = {
    selected: true,
    color: COLOR.selected
};

const UNSELECTED = {
    selected: false,
    color: COLOR.unselected
};

const KEY = 'html';
const LIST_LEVEL_LIMIT = 3;
const TOP_BLOCK_LEVEL_LIMIT = 9;
const INITIAL_TEXT = "<div><br></div>";

const TOOLS_PROPS = [
    { label: 'format_bold', type: 'bold', css:"material-icons"},
    { label: 'format_italic', type: 'italic', css:"material-icons" },
    { label: 'format_underlined', type: 'underlined', css:"material-icons" },
    { label: 'code', type: 'code', css:"material-icons" },
    { label: 'looks_one', type: 'looks_one', css:"material-icons" },
    { label: 'looks_two', type: 'looks_two', css:"material-icons" },
    { label: 'format_quote', type: 'quote', css:"material-icons" },
    { label: 'image', type: 'image', css:"material-icons" },
    { label: 'image_search', type: 'image_search', css:"material-icons" },
    { label: 'format_list_numbered', type: 'list_numbered', css:"material-icons" },
    { label: 'format_list_bulleted', type: 'list_bulleted', css:"material-icons" },
];


class Editor extends React.Component {

    constructor(props) {
        super(props);
    
        this.inputOpenFileRef = React.createRef();
        this.editableDiv = React.createRef();

        this.state = {
            tools: {
                bold: {
                    selected: false,
                    color: COLOR.unselected
                },
                italic: {
                    selected: false,
                    color: COLOR.unselected
                },
                underlined: {                 
                    selected: false,                 
                    color: COLOR.unselected             
                },
                code: {                 
                    selected: false,                 
                    color: COLOR.unselected             
                },
                looks_one: {                 
                    selected: false,                 
                    color: COLOR.unselected             
                },
                looks_two: {                 
                    selected: false,                 
                    color: COLOR.unselected             
                },
                quote: {                 
                    selected: false,                 
                    color: COLOR.unselected             
                },
                image: {                 
                    selected: false,                 
                    color: COLOR.unselected             
                },
                image_search: {                 
                    selected: false,                 
                    color: COLOR.unselected             
                },
                list_numbered: {                 
                    selected: false,                 
                    color: COLOR.unselected             
                },
                list_bulleted: {
                    selected: false,                 
                    color: COLOR.unselected
                }
            },
            editable: true,
            saveEnable: true,
            html: this.loadFromLocalStorage()
    
        }
    }

    loadFromLocalStorage = () => {
        return localStorage.hasOwnProperty(KEY) ? localStorage.getItem(KEY) : INITIAL_TEXT;
    }

    componentDidUpdate(prevProps, prevState){

        // console.log(prevProps);
        console.log(prevState.tools);
        console.log(this.state.tools);

        console.log("componentDidUpdate");

        if(prevState.tools !== this.state.tools){

            const isSelectedBold = document.queryCommandState('bold');
            const isSelectedItalic = document.queryCommandState('italic');
            const isSelectedUnderline = document.queryCommandState('underline');

            if(this.state.tools['bold'].selected!==isSelectedBold){
                    document.execCommand('bold', false);
            }

            if(this.state.tools['italic'].selected!==isSelectedItalic){
                document.execCommand('italic', false);
            }

            if( this.state.tools['underlined'].selected!==isSelectedUnderline){
                document.execCommand('underline', false);
            }
        }

    }

    actionTaken = (tags) => {

        const first = tags.shift();
        let isSelected = this.state.tools[first].selected;
        
        const updatedTools = {
            ...this.state.tools
        };

        updatedTools[first] = isSelected ? UNSELECTED : SELECTED;

        tags.forEach( (tag) => {
            updatedTools[tag] = UNSELECTED;
        });

        this.setState( { tools: updatedTools } );
    }

    onBoldClick = () => {

        const tags = ['bold'];

        // document.execCommand('bold', false);

        this.actionTaken(tags);
    }

    onItalicClick = () => {

        // document.execCommand('italic', false);

        const tags = ['italic'];

        this.actionTaken(tags);
    }

    onUnderlinedClick = () => {

        // document.execCommand('underline', false);

        const tags = ['underlined'];

        this.actionTaken(tags);
    }

    onCodeClick = () => {

        const text = window.getSelection().toString();
        
        if(text !== ""){

            const selection = window.getSelection();
            let node = selection.focusNode.parentNode;
            let tag = node.localName;

            if(tag === 'code'){
                document.execCommand('removeFormat', false);
            }else{
                document.execCommand("insertHTML", false, "<code>"+ text +"</code>");
            }

            // const isSelected = this.state.tools['code'].selected;
            
        }
        
        const tags = ['code'];

        this.actionTaken(tags);
    }

    onQuoteClick = () => {

        const selection = window.getSelection();
        let node = selection.focusNode;

        if(node.localName === undefined){
            node = node.parentNode;
        }

        const tag = node.localName;

        if(tag === 'blockquote'){
            document.execCommand('formatBlock', false, 'div');
            // this.unwrapNode(node);
        }else{
            document.execCommand('formatBlock', false, 'blockquote');
        }

        const tags = ['quote', 'image', 'looks_one', 'looks_two', 'list_numbered', 'list_bulleted'];

        this.actionTaken(tags);
    }

    onLooksOneClick = () => {

        const selection = window.getSelection();
        let node = selection.focusNode.parentNode;
        let tag = node.localName;

        if(tag === 'h1'){
            document.execCommand('formatBlock', false, 'div');
            // this.unwrapNode(node);
        }else{
            document.execCommand("formatBlock", false, 'h1');
        }
        

        const tags = ['looks_one', 'quote', 'image', 'looks_two', 'list_numbered', 'list_bulleted'];

        this.actionTaken(tags);
    }

    onLooksTwoClick = () => {

        const selection = window.getSelection();
        let node = selection.focusNode.parentNode;
        let tag = node.localName;

        if(tag === 'h2'){
            document.execCommand('formatBlock', false, 'div');
            // this.unwrapNode(node);
        }else{
            document.execCommand("formatBlock", false, 'h2');
        }

        const tags = ['looks_two', 'quote', 'image', 'looks_one', 'list_numbered', 'list_bulleted'];

        this.actionTaken(tags);
    }

    imageAction = () => {

        // const isSelected = this.state.tools['image'].selected;

        const updatedTools = {
            ...this.state.tools
        };
        updatedTools['quote'] = UNSELECTED;
        // updatedTools['image'] = isSelected ? UNSELECTED : SELECTED ;
        updatedTools['image'] = UNSELECTED ;
        updatedTools['looks_one'] = UNSELECTED;
        updatedTools['looks_two'] = UNSELECTED;
        updatedTools['list_numbered'] = UNSELECTED;
        updatedTools['list_bulleted'] = UNSELECTED;
        
        this.setState( { tools: updatedTools } );
    }

    onImageChange = (file) => {
        
        const reader  = new FileReader();
        reader.onload = function(e)  {
            const src = e.target.result;
            const image = "<img src='"+src+"' alt='image' style='max-width: 100%; max-height: 20em;' />";
            document.execCommand("insertHTML", false, image);
            
        }
        reader.readAsDataURL(file);
        
        this.imageAction();   
    }

    onImageClick = (event) => {

        this.inputOpenFileRef.current.click();
        
    }

    onImageSearchClick = (event) => {

        const url = prompt("Enter the URL of the image:");
        if (url !== null && url !== "") {
            const src = url;
            const image = "<img src='"+src+"' alt='image' style='max-width: 100%; max-height: 20em;' />";
            document.execCommand("insertHTML", false, image);
                
            this.imageAction();
        }else{
            return;
        }
        
    }

    onListNumberedClick = () => {

        document.execCommand('insertOrderedList', false);

        const tags = ['list_numbered', 'quote', 'image', 'looks_one', 'looks_two', 'list_bulleted'];

        this.actionTaken(tags);
    }

    onListBulletedClick = () => {

        document.execCommand('insertUnorderedList', false);

        const tags = ['list_bulleted', 'quote', 'image', 'looks_one', 'looks_two', 'list_numbered'];

        this.actionTaken(tags);
        
    }

    onErrorClick = () => {
        console.log('Error Clicked');
    }

    onButtonClick = (type) => {
        switch(type){
            case 'bold':
                this.onBoldClick();
                break;
            case 'italic':
                this.onItalicClick();
                break;
            case 'underlined':
                this.onUnderlinedClick();
                break;
            case 'code':
                this.onCodeClick();
                break;
            case 'quote':
                this.onQuoteClick();
                break;
            case 'looks_one':
                this.onLooksOneClick();
                break;
            case 'looks_two':
                this.onLooksTwoClick();
                break;
            case 'image':
                this.onImageClick();
                break;
            case 'image_search':
                this.onImageSearchClick();
                break;
            case 'list_numbered':
                this.onListNumberedClick();
                break;
            case 'list_bulleted':
                this.onListBulletedClick();
                break;
            default:
                this.onErrorClick();

        }
    }

    handleChange = (event) => {
        let text = event.target.value;

        if(text === '<br>' || text === ""){
            text = "<div><br></div>";
        }

        if(event.currentTarget.childElementCount > TOP_BLOCK_LEVEL_LIMIT){
            this.setState({html: text, saveEnable:false});
        }else{
            this.setState({html: text, saveEnable:true});
        }
    
        
    }

    handleKeyChange = (event) => {

        if(event.shiftKey && event.keyCode === 9) { 
            document.execCommand('outdent', false);
            event.preventDefault();
        }else if(event.keyCode === 9){
            const selection = window.getSelection();
            const node = selection.focusNode.parentNode;
            const tag = node.localName;
            let level;

            if(tag==='li'){
                level = this.getTagList().length - 1;
            }else if(tag==='ul' || tag==='ol'){
                level = this.getTagList().length;
            }

            if(level<LIST_LEVEL_LIMIT){
                document.execCommand('indent', false);
            }  

            event.preventDefault();
               
        }
        
    }

    fileChangedHandler = (event) => {
        const file = event.target.files[0];
        this.onImageChange(file);
    }

    onSaveClick = () => {
        const value = this.state.html;
        localStorage.setItem(KEY, value);
    }

    onCancelClick = () => {

        const text = this.loadFromLocalStorage();
        this.setState({html: text, saveEnable:true});

    }

    toolbarControlOnClick = (tags) => {
        const updatedTools = {
            ...this.state.tools
        };

        const keys = Object.keys(updatedTools);

        keys.forEach( (tag) => {
            updatedTools[tag] = UNSELECTED;
        });

        tags.forEach( (tag) => {
            updatedTools[tag] = SELECTED;
        });

        this.setState( { tools: updatedTools } );
    }

    manageToolbar = (tag) => {
        let result;
        switch(tag){
            case 'b':
                result = "bold";
                break;
            case 'i':
            result = "italic";
                break;
            case 'u':
                result = "underlined";
                break;
            case 'code':
                result = "code";
                break;
            case 'blockquote':
                result = "quote";
                break;
            case 'h1':
                result = "looks_one";
                break;
            case 'h2':
                result = "looks_two";
                break;
            case 'ol':
                result = "list_numbered";
                break;
            case 'ul':
                result = "list_bulleted";
                break;
            default:
                result = "";

        }

        return result;
    }

    getTagList = () => {

        let tags = [];

        const selection = window.getSelection();
        let node = selection.focusNode.parentNode
        let ownTag = node.localName;

        while(ownTag !== 'div'){
            tags.push(ownTag);
            node = node.parentNode
            ownTag = node.localName;
        }

        return tags;
    }

    handleClickInEditor = () => {
        // let results = [];
        // let res;
        // const tags = this.getTagList();
        
        // tags.forEach( (tag) => {
        //     res = this.manageToolbar(tag);

        //     if(res !== ""){
        //         results.push(res);
        //     }
        // });

        // this.toolbarControlOnClick(results);
        console.log("handleClickInEditor");
        
    }

    render() {
        return (
            <Aux>
                <Action 
                    save={this.onSaveClick}
                    cancel={this.onCancelClick}
                    enabled = {this.state.saveEnable}
                />
                <Toolbar 
                tools={this.state.tools}
                tools_props={TOOLS_PROPS}
                clicked={this.onButtonClick} />
                <Notebook 
                    editable={this.state.editable}
                    html={this.state.html}
                    handleChange={this.handleChange}
                    handleKeyChange={this.handleKeyChange}
                    clickInEditor={this.handleClickInEditor}
                    editableDivRef = {this.editableDiv}
                    />
                <Input 
                    inputOpenFileRef={this.inputOpenFileRef}
                    type="file" 
                    inputClass="Image"
                    fileChangedHandler={this.fileChangedHandler}
                    accept="image/*"
                />
            </Aux>
        );
    }
}

export default Editor;