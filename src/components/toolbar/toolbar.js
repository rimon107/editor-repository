import React from 'react';
import './toolbar.css';
import Button from '../ui/button/button';

const toolbar = (props) => {

    const tools = props.tools_props.map(ctrl => (
                            <div className="CenterDiv" key={ctrl.label}>
                                <Button btnType="" key={ctrl.label} clicked={() => props.clicked(ctrl.type)}>
                                    <span className={Object.keys(props.tools)
                                        .map(tKey => {
                                            const val = props.tools[tKey];
                                            return  ctrl.type === tKey ? val.color : '';
                                        }).reduce((val, el) => 
                                        {
                                            return val += el;
                                        },'')
                                        }><span className={ctrl.css}>{ctrl.label}</span></span>
                                </Button>
                            </div>
                ));

    return(
        <div className="Toolbar">
            {tools}            
        </div>
    );
};

export default toolbar;