import React from 'react';
import deepEqual from 'fast-deep-equal';
import PropTypes from 'prop-types';

const normalizeHtml = (str) => {

    return str && str.replace(/&nbsp;|\u202F|\u00A0/g, ' ');

}

const findLastTextNode = (node) => {
    if (node.nodeType === Node.TEXT_NODE) return node;
    let children = node.childNodes;
    for (let i = children.length - 1; i >= 0; i--) {
      let textNode = findLastTextNode(children[i]);
      if (textNode !== null) return textNode;
    }
    return null;
}

const replaceCaret = (el) => {
  // Place the caret at the end of the element
  const target = findLastTextNode(el);
  // do not move caret if element was not focused
  const isTargetFocused = document.activeElement === target;
  if (target !== null && target.nodeValue !== null && isTargetFocused) {
    var range = document.createRange();
    var sel = window.getSelection();
    range.setStart(target, target.nodeValue.length);
    range.collapse(true);
    sel.removeAllRanges();
    sel.addRange(range);
    if (el instanceof HTMLElement) el.focus();
  }
};

const setCaretPosition = () => {

  let sel;
  // content.focus();
  if (document.selection) {

  } else {
    sel = window.getSelection();

    const r = sel.getRangeAt(0);
    sel.removeAllRanges();
    sel.addRange(r);

  }


}



/**
 * A simple component for an html element with editable contents.
 */
class ContentEditable extends React.Component {

  lastHtml = this.props.html;
  el = typeof this.props.innerRef === 'function' ? {
    current: null
  } : React.createRef();

  getEl = () => (this.props.innerRef && typeof (this.props.innerRef) !== 'function' ? this.props.innerRef : this.el).current;

  shouldComponentUpdate(nextProps) {
    const {
      props
    } = this;
    const el = this.getEl();

    // We need not rerender if the change of props simply reflects the user's edits.
    // Rerendering in this case would make the cursor/caret jump

    // Rerender if there is no element yet... (somehow?)
    if (!el) return true;

    // ...or if html really changed... (programmatically, not by user edit)
    if (
      normalizeHtml(nextProps.html) !== normalizeHtml(el.innerHTML)
    ) {
      return true;
    }

    // Handle additional properties
    return props.disabled !== nextProps.disabled ||
      props.tagName !== nextProps.tagName ||
      props.className !== nextProps.className ||
      props.innerRef !== nextProps.innerRef ||
      !deepEqual(props.style, nextProps.style);
  }

  componentDidUpdate() {
    const el = this.getEl();
    if (!el) return;

    // Perhaps React (whose VDOM gets outdated because we often prevent
    // rerendering) did not update the DOM. So we update it manually now.
    if (this.props.html !== el.innerHTML) {
      el.innerHTML = this.lastHtml = this.props.html;
    }
    replaceCaret(el);
  }

  keepFocus = (event) => {
    setCaretPosition();
  };

  emitChange = (originalEvt) => {
    const el = this.getEl();
    if (!el) return;

    const html = el.innerHTML;
    if (this.props.onChange && html !== this.lastHtml) {
      // Clone event with Object.assign to avoid
      // "Cannot assign to read only property 'target' of object"
      const evt = Object.assign({}, originalEvt, {
        target: {
          value: html
        }
      });
      this.props.onChange(evt);
    }
    this.lastHtml = html;
  }

  render() {
    const {
      tagName,
      html,
      innerRef,
      ...props
    } = this.props;

    return React.createElement(
      tagName || 'div', {
        ...props,
        ref: typeof innerRef === 'function' ? (current) => {
          innerRef(current)
          this.el.current = current
        } : innerRef || this.el,
        onInput: this.emitChange,
        onClick: this.props.onClick,
        onBlur: this.keepFocus,
        onKeyDown: this.props.onKeyDown,
        contentEditable: !this.props.disabled,
        dangerouslySetInnerHTML: {
          __html: html
        }
      }
      // ,
      // this.props.children
    );
  }




}

ContentEditable.propTypes = {
  html: PropTypes.string,
  onInput: PropTypes.func,
  onClick: PropTypes.func,
  onBlur: PropTypes.func,
  onKeyDown: PropTypes.func,
  disabled: PropTypes.bool,
  tagName: PropTypes.string,
  className: PropTypes.string,
  style: PropTypes.object,
  innerRef: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.func,
  ])
}

export default ContentEditable;