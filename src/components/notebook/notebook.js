import React from 'react';
import './notebook.css';
import ContentEditable from "./content-editable/content-editable";



const notebook = (props) => {

    return(

        <ContentEditable 
            className="editable"
            html={props.html} // innerHTML of the editable div
            disabled={!props.editable}        // use true to disable editing
            onChange={props.handleChange} // handle innerHTML change
            onInput={props.handleChange} // handle innerHTML change
            tagName='div' // Use a custom HTML tag (uses a div by default)
            onClick={props.clickInEditor}
            innerRef={props.editableDivRef}
            onKeyDown={props.handleKeyChange}
            />
        
    );
};

export default notebook;