import React from 'react';
import './input.css'

class Input extends React.Component{

    render(){
        return(
            <input
                className={this.props.inputClass}
                type={this.props.type}
                ref={this.props.inputOpenFileRef}
                onChange={this.props.fileChangedHandler}
                accept={this.props.accept}
            />
        )
    }
} 

export default Input;