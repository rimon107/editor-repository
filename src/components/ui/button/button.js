import React from 'react';

import './button.css';

const Button = (props) => (
    <button
        className={ "Button " + props.btnType}
        disabled={props.disabled}
        onClick={props.clicked}>{props.children}
    </button>
);

export default Button;