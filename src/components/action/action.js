import React from 'react';
import './action.css';
import Button from '../ui/button/button';

const action = (props) =>  {
    const css = props.enabled ? "Success" : "Disable";

    return(
        <div className="Center">
            <Button key="save" btnType={css} disabled={!props.enabled} clicked={props.save}>Save</Button>
            <Button key="cancel" btnType="Danger" disabled={false} clicked={props.cancel}>Cancel</Button>
        </div>
    );
    
}

export default action;

